package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"sync"
)

func Swap(sli []int, idx int) {
	sli[idx], sli[idx+1] = sli[idx+1], sli[idx]
}

func BubbleSort(sli []int) {
	for {
		numSwaps := 0
		for i, v := range sli {
			if i == len(sli)-1 {
				break
			}
			if v > sli[i+1] {
				Swap(sli, i)
				numSwaps++
			}
		}
		if numSwaps == 0 {
			break
		}
	}
}

func SortGroup(grp []int, wg *sync.WaitGroup) {
	fmt.Printf("Now sorting: %v\n", grp)
	BubbleSort(grp)
	wg.Done()
}

func main() {
	sli := make([]int, 0)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Enter a series of integers (press enter when done):\n")
	for sliIdx := 0; scanner.Scan(); sliIdx++ {
		input := scanner.Text()
		if input == "" {
			break
		}
		num, err := strconv.Atoi(input)
		if err != nil {
			fmt.Printf("There was an error: %s\n", err)
		} else {
			sli = append(sli, num)
		}
	}
	fmt.Printf("Here is your unsorted array: %v\n", sli)
	var wg sync.WaitGroup
	wg.Add(4)
	idx2 := len(sli) / 2
	idx1 := idx2 / 2
	idx3 := idx1 + idx2
	go SortGroup(sli[:idx1], &wg)
	go SortGroup(sli[idx1:idx2], &wg)
	go SortGroup(sli[idx2:idx3], &wg)
	go SortGroup(sli[idx3:], &wg)
	wg.Wait()
	BubbleSort(sli)
	fmt.Printf("Here is your sorted array: %v\n", sli)
}

/*
Write a program to sort an array of integers.
The program should partition the array into 4 parts,
each of which is sorted by a different goroutine.
Each partition should be of approximately equal size.
Then the main goroutine should merge the 4 sorted subarrays into one large sorted array.

The program should prompt the user to input a series of integers.
Each goroutine which sorts ¼ of the array should print the subarray that it will sort.
When sorting is complete, the main goroutine should print the entire sorted list.
*/
