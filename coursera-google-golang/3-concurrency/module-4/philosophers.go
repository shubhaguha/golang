package main

import (
	"fmt"
	"sync"
)

// Each chopstick is a mutex.
type Chopstick struct{ sync.Mutex }

// Each philosopher is associated with a goroutine and two chopsticks.
type Philosopher struct {
	id                    int
	leftStick, rightStick *Chopstick
}

// Each philosopher is associated with a goroutine and two chopsticks.
func (p Philosopher) eat(c chan int, wg *sync.WaitGroup) {
	for bite := 0; bite < 3; bite++ {
		c <- 1
		p.leftStick.Lock()
		p.rightStick.Lock()
		fmt.Printf("starting to eat %d\n", p.id)
		fmt.Printf("finishing eating %d\n", p.id)
		p.leftStick.Unlock()
		p.rightStick.Unlock()
		<-c
	}
	wg.Done()
}

func main() {
	// Each chopstick is a mutex.
	chopsticks := make([]*Chopstick, 5)
	for i := 0; i < 5; i++ {
		chopsticks[i] = new(Chopstick)
	}

	// Each philosopher is associated with a goroutine and two chopsticks.
	philosophers := make([]*Philosopher, 5)
	for i := 0; i < 5; i++ {
		philosophers[i] = &Philosopher{i + 1, chopsticks[i], chopsticks[(i+1)%5]}
	}

	philChan := make(chan int, 2)
	var wg sync.WaitGroup
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go philosophers[i].eat(philChan, &wg)
	}
	wg.Wait()
}

/*
Implement the dining philosopher’s problem with the following constraints/modifications.

There should be 5 philosophers sharing chopsticks, with one chopstick between each adjacent pair of philosophers.
Each philosopher should eat only 3 times (not in an infinite loop as we did in lecture)
The philosophers pick up the chopsticks in any order, not lowest-numbered first (which we did in lecture).
In order to eat, a philosopher must get permission from a host which executes in its own goroutine.
The host allows no more than 2 philosophers to eat concurrently.
Each philosopher is numbered, 1 through 5.
When a philosopher starts eating (after it has obtained necessary locks)
it prints “starting to eat <number>” on a line by itself, where <number> is the number of the philosopher.
When a philosopher finishes eating (before it has released its locks)
it prints “finishing eating <number>” on a line by itself, where <number> is the number of the philosopher.
*/
