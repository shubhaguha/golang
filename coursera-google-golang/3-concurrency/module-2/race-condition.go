package main

import (
	"fmt"
	"sync"
)

func increment(c *int, wg *sync.WaitGroup) {
	*c = *c + 1
	fmt.Printf("Incremented:\tc = %d\n", *c)
	wg.Done()
}

func double(c *int, wg *sync.WaitGroup) {
	*c = *c * 2
	fmt.Printf("Doubled:\tc = %d\n", *c)
	wg.Done()
}

func main() {
	x := 5
	var wg sync.WaitGroup
	wg.Add(2)
	go increment(&x, &wg)
	go double(&x, &wg)
	wg.Wait()
	fmt.Printf("Final result\tx = %d\n", x)
}

/*
Explanation:

Race conditions occur when the result of a program is nondeterministic based on
the order of interleaving of concurrent tasks. For example, when concurrent
tasks access and/or change the same data, they can influence each other’s
outcomes. However, when tasks are defined as concurrent, for example by using
goroutines, they can be executed by the Go Runtime Scheduler in any order
because as separate goroutines it is implied that they are independent of each
other.

In my example here, the final value of the variable x can be 11 or 12 based on
which of the two goroutines executes first. The printed output shows the order
of execution: the final value is 11 if increment() is executed after double()
but it is 12 if double() is executed after increment().
*/
