package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Enter a string: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	lower := strings.ToLower(scanner.Text())
	if strings.HasPrefix(lower, "i") && strings.Contains(lower, "a") && strings.HasSuffix(lower, "n") {
		fmt.Println("Found!")
	} else {
		fmt.Println("Not found!")
	}
}
