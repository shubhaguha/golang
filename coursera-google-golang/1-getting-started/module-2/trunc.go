package main

import "fmt"

func main() {
	var f float64
	fmt.Printf("Please enter a floating point number: ")
	fmt.Scan(&f)
	fmt.Println(int(f))
}
