Module 2 Quiz
===

Question 5
---

	func main() {
		var xtemp int
		x1 := 0
		x2 := 1
		for x:=0; x<5; x++ {
			xtemp = x2
			x2 = x2 + x1
			x1 = xtemp
		}
		fmt.Println(x2)
	}

	x = 0
	xtemp = 1
	x2 = 1
	x1 = 1

	x = 1
	xtemp = 1
	x2 = 2
	x1 = 1

	x = 2
	xtemp = 2
	x2 = 3
	x1 = 2

	x = 3
	xtemp = 3
	x2 = 5
	x1 = 3

	x = 4
	xtemp = 5
	x2 = 8
	x1 = 5

	8
