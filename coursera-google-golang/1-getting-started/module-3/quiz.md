Module 3 Quiz
===

Question 2
---

x is array [4 8 5]

y is slice [4 8]

z is slice [8 5]

	y[0] = 1

x is [1 8 5]

	z[1] = 3

x is [1 8 3]

Question 3
---

x is array [1 2 3 4 5]

y is slice [1 2]

- len 2
- cap 5

z is slice [2 3 4]

- len 3
- cap 4
