package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	sli := make(sort.IntSlice, 0, 3)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Enter an integer: ")
	for scanner.Scan() {
		input := scanner.Text()
		if strings.EqualFold(input, "X") {
			break
		}
		i, err := strconv.Atoi(input)
		if err != nil {
			fmt.Printf("There was an error: %s", err)
		} else {
			sli = append(sli, i)
			sli.Sort()
			fmt.Println(sli)
		}
		fmt.Printf("Enter an integer: ")
	}
}
