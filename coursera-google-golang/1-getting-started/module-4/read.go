package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Name struct {
	fname string
	lname string
}

func main() {
	fmt.Printf("Filename: ")
	inputScanner := bufio.NewScanner(os.Stdin)
	inputScanner.Scan()
	filename := inputScanner.Text()
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("There was an error while opening the file: %s", err)
	}
	fileScanner := bufio.NewScanner(file)
	sli := make([]Name, 0, 5)
	for fileScanner.Scan() {
		fields := strings.Fields(fileScanner.Text())
		if len(fields) != 2 {
			continue
		}
		sli = append(sli, Name{fname: fields[0], lname: fields[1]})
	}
	for _, name := range sli {
		fmt.Println(name.fname, name.lname)
	}
}
