package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	idMap := make(map[string]string)
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Enter name: ")
	scanner.Scan()
	idMap["name"] = scanner.Text()
	fmt.Printf("Enter address: ")
	scanner.Scan()
	idMap["address"] = scanner.Text()
	byteArr, err := json.Marshal(idMap)
	if err != nil {
		fmt.Printf("There was an error: %s", err)
		return
	}
	fmt.Println(string(byteArr))
}
