/*
Write a Bubble Sort program in Go.
The program should prompt the user to type in a sequence of up to 10 integers.
The program should print the integers out on one line, in sorted order, from least to greatest.
Use your favorite search tool to find a description of how the bubble sort algorithm works.

As part of this program, you should write a function called BubbleSort()
which takes a slice of integers as an argument and returns nothing.
The BubbleSort() function should modify the slice so that the elements are in sorted order.

A recurring operation in the bubble sort algorithm is the Swap operation
which swaps the position of two adjacent elements in the slice.
You should write a Swap() function which performs this operation.
Your Swap() function should take two arguments, a slice of integers and an index value i
which indicates a position in the slice. The Swap() function should return nothing,
but it should swap the contents of the slice in position i with the contents in position i+1.
*/

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func Swap(sli []int, idx int) {
	sli[idx], sli[idx+1] = sli[idx+1], sli[idx]
}

func BubbleSort(sli []int) {
	for {
		numSwaps := 0
		for i, v := range sli {
			if i == len(sli)-1 {
				break
			}
			if v > sli[i+1] {
				Swap(sli, i)
				numSwaps++
			}
		}
		if numSwaps == 0 {
			break
		}
	}
}

func main() {
	sli := make([]int, 0, 10)
	fmt.Printf("Enter up to 10 integers, separated by spaces: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()
	fields := strings.Fields(text)
	for _, v := range fields {
		if len(sli) == 10 {
			break
		}
		num, err := strconv.Atoi(v)
		if err != nil {
			continue
		}
		sli = append(sli, num)
	}
	BubbleSort(sli)
	fmt.Println(sli)
}
