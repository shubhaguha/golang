package main

import (
	"fmt"
	"math"
)

func GenDisplaceFn(a, v0, s0 float64) func(float64) float64 {
	return func(t float64) float64 {
		return (0.5 * a * math.Pow(t, 2)) + (v0 * t) + (s0)
	}
}

func main() {
	var a, v0, s0, t float64
	fmt.Printf("Enter acceleration: ")
	fmt.Scan(&a)
	fmt.Printf("Enter initial velocity: ")
	fmt.Scan(&v0)
	fmt.Printf("Enter initial displacement: ")
	fmt.Scan(&s0)
	fn := GenDisplaceFn(a, v0, s0)

	fmt.Printf("Enter time: ")
	fmt.Scan(&t)

	fmt.Printf("Displacement at time %.0f = %.0f\n", t, fn(t))
}

/*
	fn := GenDisplaceFn(10, 2, 1)

	t = 3
	(0.5 * 10 * 3^2) + (2 * 3) + 1 = 52

	t = 5
	(0.5 * 10 * 5^2) + (2 * 5) + 1 = 136

	fn2 := GenDisplaceFn(3, 7, -5)

	t = 2
	(0.5 * 3 * 2^2) + (7 * 2) - 5 = 15

	t = 6
	(0.5 * 3 * 6^2) + (7 * 6) - 5 = 91
*/
