package main

import (
	"fmt"
	"greet"             // must set GOPATH to project dir
	"greet/de"          // must set GOPATH to project dir
	child "greet/greet" // alias to avoid conflicting packages named `greet`
)

func init() {
	fmt.Println("app/entry.go ==> init()")
}

func init() {
	fmt.Println("app/entry.go ==> demonstration of multiple inits in same file executed in declaration order")
}

func main() {
	fmt.Println(greet.Morning)
	fmt.Println(de.Morning) // package name `de` from `package de` declaration
	fmt.Println("version ==> ", version)
	fmt.Println(child.Message)
}
