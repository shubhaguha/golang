package main

import "fmt"

var version = "1.0.0"

// can use variable from anywhere in package even though it is not exported (i.e. Version)
// because it is declared in package scope

func init() {
	fmt.Println("app/version.go ==> demonstration of multiple inits in same package executed in file alphabetical order")
}
