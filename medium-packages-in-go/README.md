3 August 2020
===

Scratch pad for Medium post ["Everything you need to know about Packages in Go"](https://medium.com/rungo/everything-you-need-to-know-about-packages-in-go-b8bac62b74cc)

Compile and execute:

```bash
export GOPATH="$PWD"
go run app
```

Compile and create binary executable (in `bin/` dir):

```bash
go install app
```

or compile and create package archive (in `pkg/` dir):

```bash
go install greet
go install greet/de
```

`package main` declaration and `func main` definition indicates **executable** package vs. `package greet` declaration indicates **utility** package.

VS Code's Go plugin compiles package on save.
