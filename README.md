# golang

Repo for my Golang learning: courses, tutorials, experimental projects that have no repo of their own.

- [Programming with Google Go (Coursera specialization)](https://www.coursera.org/specializations/google-golang)
- [Go Web Examples](https://gowebexamples.com/)
- [Building Web Apps with Go (Code Gangsta book)](https://codegangsta.gitbooks.io/building-web-apps-with-go/)
