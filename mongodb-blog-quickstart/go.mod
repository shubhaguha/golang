module quickstart

go 1.14

require (
	go.mongodb.org/mongo-driver v1.4.1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
