MongoDB Blog Quickstart Tutorials
===

7 September 2020

[Starting and Setup](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--starting-and-setup)
---

Posted 7 November 2019.

```bash
go mod init quickstart
go get go.mongodb.org/mongo-driver
MONGO_URI="mongodb://localhost:27017" go run main.go
```

[How to Create Documents](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--how-to-create-documents) (**C**RUD)
---

Posted 14 November 2019.

In this particular part of the series, primitive data structures were used to save us from having to pre-define a schema.

- `InsertOne`
- `InsertMany`

[How to Read Documents](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--how-to-read-documents) (C**R**UD)
---

Posted 21 November 2019.

- `ListDatabaseNames` (from first tutorial)
- `Find`
- `FindOne`
- `FindOptions` (for sorting, etc.)

[How to Update Documents](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--how-to-update-documents) (CR**U**D)
---

Posted 5 December 2019.

Need filter criteria + update operation.

- `UpdateOne`
- `UpdateMany`
- `ReplaceOne`

[A Quick Look at GridFS](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--a-quick-look-at-gridfs)
---

Posted 19 December 2019.

In this tutorial, we’re going to look at how to use the MongoDB GridFS feature to store files which are larger than 16 MB. Some use case of GridFS are:

If your filesystem limits the number of files in a directory, you can use GridFS to store as many files as needed.
When you want to access information from portions of large files without having to load files into memory, you can use GridFS to recall sections of files without reading the entire file into memory.
When you want to keep your files and metadata automatically synced and deployed across a number of systems and facilities, you can use GridFS. When using geographically distributed replica sets, MongoDB can distribute files and their metadata automatically to a number of mongod instances and facilities.
For more information about GridFS, head over to [this page](https://docs.mongodb.com/manual/core/gridfs/#faq-developers-when-to-use-gridfs).

```bash
go get go.mongodb.org/mongo-driver/mongo
go get go.mongodb.org/mongo-driver/mongo/gridfs
```

In `main.go`:

```go
func main() {
    conn := InitiateMongoClient()
    bucket, err := gridfs.NewBucket(
        conn.Database("myfiles"),
    )
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
}

func InitiateMongoClient() *mongo.Client {
    var err error
    var client *mongo.Client
    uri := "mongodb://localhost:27017"
    opts := options.Client()
    opts.ApplyURI(uri)
    opts.SetMaxPoolSize(5)
    if client, err = mongo.Connect(context.Background(), opts); err != nil {
        fmt.Println(err.Error())
    }
    return client
}

func UploadFile(file, filename string) {
    data, err := ioutil.ReadFile(file)
    if err != nil {
        log.Fatal(err)
    ...
    }

    uploadStream, err := bucket.OpenUploadStream(
        filename, // this is the name of the file which will be saved in the database
    )
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    defer uploadStream.Close()

    fileSize, err := uploadStream.Write(data)
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
    log.Printf("Write file to DB was successful. File size: %d \n", fileSize)
}
```

When a file is uploaded to MongoDB, two collections are automatically created in database: fs.chunks and fs.files. All the metadata such as file size, file type, file name are stored in fs.files collection and the file itself is saved into multiple chunks under the same ID in chunks.fs.

```go
func DownloadFile(fileName string) {
    conn := InitiateMongoClient()

    // For CRUD operations, here is an example
    db := conn.Database("myfiles")
    fsFiles := db.Collection("fs.files")
    ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
    var results bson.M
    err := fsFiles.FindOne(ctx, bson.M{}).Decode(&results)
    if err != nil {
        log.Fatal(err)
    }
    // you can print out the result
    fmt.Println(results)

    bucket, _ := gridfs.NewBucket(
        db,
    )
    var buf bytes.Buffer
    dStream, err := bucket.DownloadToStreamByName(fileName, &buf)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Printf("File size to download: %v \n", dStream)
    ioutil.WriteFile(fileName, buf.Bytes(), 0600)
}
```

Full program:

```go
package main

import (
    "bytes"
    "context"
    "fmt"
    "io/ioutil"
    "log"
    "os"
    "path"
    "time"

    "go.mongodb.org/mongo-driver/bson"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/gridfs"
    "go.mongodb.org/mongo-driver/mongo/options"
)

func InitiateMongoClient() *mongo.Client {
    var err error
    var client *mongo.Client
    uri := "mongodb://localhost:27017"
    opts := options.Client()
    opts.ApplyURI(uri)
    opts.SetMaxPoolSize(5)
    if client, err = mongo.Connect(context.Background(), opts); err != nil {
        fmt.Println(err.Error())
    }
    return client
}

func UploadFile(file, filename string) {
    data, err := ioutil.ReadFile(file)
    if err != nil {
        log.Fatal(err)
    }
    conn := InitiateMongoClient()
    bucket, err := gridfs.NewBucket(
        conn.Database("myfiles"),
    )
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
    uploadStream, err := bucket.OpenUploadStream(
        filename,
    )
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    defer uploadStream.Close()

    fileSize, err := uploadStream.Write(data)
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
    log.Printf("Write file to DB was successful. File size: %d M\n", fileSize)
}

func DownloadFile(fileName string) {
    conn := InitiateMongoClient()

    // For CRUD operations, here is an example
    db := conn.Database("myfiles")
    fsFiles := db.Collection("fs.files")
    ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
    var results bson.M
    err := fsFiles.FindOne(ctx, bson.M{}).Decode(&results)
    if err != nil {
        log.Fatal(err)
    }
    // you can print out the results
    fmt.Println(results)

    bucket, _ := gridfs.NewBucket(
        db,
    )
    var buf bytes.Buffer
    dStream, err := bucket.DownloadToStreamByName(fileName, &buf)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Printf("File size to download: %v\n", dStream)
    ioutil.WriteFile(fileName, buf.Bytes(), 0600)
}

func main() {
    // Get os.Args values
    file := os.Args[1] //os.Args[1] = testfile.zip
    filename := path.Base(file)
    UploadFile(file, filename)
    // Uncomment the below line and comment the UploadFile above this line to download the file
    //DownloadFile(filename)
}
```

To run:

```bash
go run main.go testfile.zip
```

[How to Delete Documents](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--how-to-delete-documents) (CRU**D**)
---

Posted 9 January 2020.

- `DeleteOne`
- `DeleteMany`
- `Drop`

[Modeling Documents with Go Data Structures](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--modeling-documents-with-go-data-structures)
---

Posted 6 February 2020.

```go
type Podcast struct {
    ID     primitive.ObjectID `bson:"_id,omitempty"`
    Title  string             `bson:"title,omitempty"`
    Author string             `bson:"author,omitempty"`
    Tags   []string           `bson:"tags,omitempty"`
}
```

The above data structure is nearly identical to the bson.M that was used previously, with the exception that it has an ID field. You'll notice the BSON annotations. These annotations are the actual fields that would appear in the document within MongoDB. The omitempty means that if there is no data in the particular field, when saved to MongoDB the field will not exist on the document rather than existing with an empty value.

Some of the benefits to using native Go data structures to represent our data is having autocomplete available, error handling, and being able to write methods specific to the data structure.

[Data Aggregation Pipeline](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--data-aggregation-pipeline)
---

Posted 20 February 2020.

- `Aggregate`

Example 1:

```go
id, _ := primitive.ObjectIDFromHex("5e3b37e51c9d4400004117e6")

matchStage := bson.D{{"$match", bson.D{{"podcast", id}}}}
groupStage := bson.D{{"$group", bson.D{{"_id", "$podcast"}, {"total", bson.D{{"$sum", "$duration"}}}}}}

showInfoCursor, err := episodesCollection.Aggregate(ctx, mongo.Pipeline{matchStage, groupStage})
if err != nil {
    panic(err)
}
var showsWithInfo []bson.M
if err = showInfoCursor.All(ctx, &showsWithInfo); err != nil {
    panic(err)
}
fmt.Println(showsWithInfo)
// result: [map[_id:ObjectID("5e3b37e51c9d4400004117e6") total:55]]
```

Example 2:

```go
lookupStage := bson.D{{"$lookup", bson.D{{"from", "podcasts"}, {"localField", "podcast"}, {"foreignField", "_id"}, {"as", "podcast"}}}}
unwindStage := bson.D{{"$unwind", bson.D{{"path", "$podcast"}, {"preserveNullAndEmptyArrays", false}}}}

showLoadedCursor, err := episodesCollection.Aggregate(ctx, mongo.Pipeline{lookupStage, unwindStage})
if err != nil {
    panic(err)
}
var showsLoaded []bson.M
if err = showLoadedCursor.All(ctx, &showsLoaded); err != nil {
    panic(err)
}
fmt.Println(showsLoaded)
// result: [map[_id:ObjectID("5e3b381c1c9d4400004117e7") description:The first episode duration:25 podcast:map[_id:ObjectID("5e3b37e51c9d4400004117e6") author:Nic Raboy name:The Polyglot Developer Podcast tags:[development coding programming]] title:Episode #1] map[_id:ObjectID("5e3b38511c9d4400004117e8") description:The second episode duration:30 podcast:map[_id:ObjectID("5e3b37e51c9d4400004117e6") author:Nic Raboy name:The Polyglot Developer Podcast tags:[development coding programming]] title:Episode #2]]
```

Example 3:

```go
// PodcastEpisode represents an aggregation result-set for two collections
type PodcastEpisode struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	Podcast     Podcast            `bson:"podcast,omitempty"`
	Title       string             `bson:"title,omitempty"`
	Description string             `bson:"description,omitempty"`
	Duration    int32              `bson:"duration,omitempty"`
}

// ...
lookupStage := bson.D{{"$lookup", bson.D{{"from", "podcasts"}, {"localField", "podcast"}, {"foreignField", "_id"}, {"as", "podcast"}}}}
unwindStage := bson.D{{"$unwind", bson.D{{"path", "$podcast"}, {"preserveNullAndEmptyArrays", false}}}}

showLoadedStructCursor, err := episodesCollection.Aggregate(ctx, mongo.Pipeline{lookupStage, unwindStage})
if err != nil {
    panic(err)
}
var showsLoadedStruct []PodcastEpisode  // HERE is the difference
if err = showLoadedStructCursor.All(ctx, &showsLoadedStruct); err != nil {
    panic(err)
}
fmt.Println(showsLoadedStruct)
```
